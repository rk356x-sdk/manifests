# Build RK356X-SDK

Recommended build host is Ubuntu 20.04 (amd64).

## Build environment setup

### Install essential tools

<pre>
  mkdir -p ~/bin
  wget 'https://storage.googleapis.com/git-repo-downloads/repo' -P ~/bin
  chmod +x ~/bin/repo
</pre>

### Setup environment variables

Add the following lines to the  ~/.bashrc file

<pre>
  export PATH=~/bin:$PATH
</pre>

And execute the following commands.

<pre>
  source ~/.bashrc
</pre>

## Download the source

<pre>
  mkdir -p /data/rk356x-sdk
  cd /data/rk356x-sdk
  repo init -u git@gitlab.com:rk356x-sdk/manifests.git -m rk356x_linux_release_v1.0.0_20210410.xml
  repo sync
</pre>

